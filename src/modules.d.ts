/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

declare module "~plugins" {
    const plugins: { [id: string]: () => import("types").Plugin };
    export default plugins;
}
