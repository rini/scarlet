/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import "./client";

export * as Api from "api";
export { Plugins } from "client";
export * as Client from "client";
export * as Utils from "utils";

export const VERSION = SC_VERSION;
