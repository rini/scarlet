/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Builtins } from "api";
import { set, update } from "api/store";
import { Accessor, createEffect, createSignal, JSXElement, on, Setter } from "solid-js";
import { createStore, SetStoreFunction, Store } from "solid-js/store";
import { render as solidRender } from "solid-js/web";
import { Plugin, Settings } from "types";
import { debounce, logger } from "utils";

import plugins from "~plugins";

const log = logger("Client");

export const Plugins = {} as Record<string, Plugin>;

const [loaded, setLoaded] = createSignal(false);

export { loaded };

const getElement = (el: Element | string, parent: ParentNode = document) =>
    typeof el == "string"
        ? parent.querySelector(el)
        : el;

const noop = () => {
    throw Error("hook called outside `start`!");
};

interface MountTarget {
    target: string | Element;
    anchor?: string | Element;
}

export let patch: (
    find: string | RegExp,
    replace: string | ((match: string, ...args: any[]) => string)
) => void = noop;
export let match: string["match"] = noop;
export let mount: (element: JSXElement, target: MountTarget) => Element = noop;
export let render: (code: () => JSXElement, target: MountTarget) => void = noop;

export let settings: Accessor<Settings>;
export let setSettings: Setter<Settings>;

let plugin: string | undefined;

export function createPluginStore<T extends object>(init: T) {
    if (!plugin)
        throw Error("resource called outside of top-level!");

    const [state, setState] = createStore(settings().plugins[plugin] = {
        ...init,
        ...settings().plugins[plugin],
    });

    // we can't really use `createEffect` because `store` itself isn't reactive (it's just a variable)
    // a little trolling must be done ..
    const wrappedSetState: SetStoreFunction<T> = (...args: any) => {
        // @ts-expect-error overload moment
        setState(...args);
        setSettings((s) => s);
    };

    return [state, wrappedSetState] as [Store<T>, SetStoreFunction<T>];
}

const defaultSettings = {
    enabledPlugins: new Set(["Skinning", "Web"]),
    plugins: {},
};

const pluginsReady = update("_settings", (v) => ({ ...defaultSettings, ...v })).then((v) => {
    [settings, setSettings] = createSignal(v, { equals: false });

    createEffect(on(
        settings,
        debounce((v) => set("_settings", v), 500),
        { defer: true }
    ));

    for (plugin in plugins)
        Plugins[plugin] = plugins[plugin]();

    plugin = undefined;
});

const originalEval = window.eval;

window.eval = (src) => {
    window.eval = originalEval;

    let plugin: string;

    patch = (find, repl) => {
        if (typeof find == "string" ? !src.match(find) : !find.test(src)) {
            log.e(`Patch does not match anything: '${find}'`);
        } else {
            if (typeof repl == "string")
                repl = repl.replaceAll("$this", `Scarlet.Plugins.${plugin}`);
            src = src.replace(find, repl as any);
        }
    };

    match = (find) => src.match(find as any) ?? (log.w(`Match doesn't work: '${find}'`), null);

    mount = (element, { target, anchor }) => {
        const parent = getElement(target);
        if (!parent) {
            log.e(`Failed to mount to '${target}', no such element`);
            return element as Element;
        }
        parent.insertBefore(element as Element, anchor ? getElement(anchor, parent) : null);
        return element as Element;
    };

    render = (component, { target, anchor }) => {
        const parent = getElement(target);
        if (!parent) {
            log.e(`Failed to render to '${parent}', no such element`);
            return;
        }
        const frag = new DocumentFragment();

        solidRender(component, frag);
        parent.insertBefore(frag, anchor ? getElement(anchor) : null);
    };

    pluginsReady.then(() => {
        for (plugin in Plugins) {
            if (!Builtins.has(plugin) && !settings().enabledPlugins.has(plugin))
                continue;

            log.d(`Applying patches from ${plugin}`);
            Plugins[plugin].start?.();
        }

        src += "\n//# sourceURL=js/tetrio.js";

        originalEval.call(undefined, src);

        for (plugin in Plugins)
            Plugins[plugin].loaded?.();

        setLoaded(true);
        log.i("Loaded!");

        patch = match = mount = render = noop;
    });
};
