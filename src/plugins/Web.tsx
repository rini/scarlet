/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { mount, patch } from "client";
import { definePlugin } from "types";
import { bre } from "utils";

export default definePlugin({
    name: "Web Extras",
    description: "Brings some desktop client features to the web version",
    authors: ["Rini"],

    start() {
        // adblock option
        patch(
            bre`window.IS_ELECTRON&&\(\?=\w\+.electron.adblock||\w\+.loadProvider()\)`,
            ""
        );
        patch(
            bre`window.IS_ELECTRON\(\?=?document.body.classList.add("no_login_ceriad")\)`,
            "true"
        );
        patch(
            bre`!!window.IS_ELECTRON&&\(\?=!!\w\+.electron.adblock\)`,
            ""
        );
        // just in case...
        patch(
            bre`(?g)_paq.push(["trackEvent","Advertising","Block","\.\*\?"])`,
            ""
        );
        // autologin option
        patch(
            bre`\(\?<=\w\+||\)window.IS_ELECTRON\(\?=&&"never"!==\w\+.electron.loginskip\)`,
            "true"
        );

        const ClientSettings = <div class="scroller_block collapsible collapsed ns">
            <h1 class="collapse_target rg_target_pri"
                title="Settings based on the desktop client"
                data-hover="tap" data-hit="click">
                CLIENT SETTINGS
            </h1>
            {document.getElementById("electron_loginskip_never")?.parentElement}
            {document.getElementById("config_electron_adblock")}
        </div>;

        mount(ClientSettings, { target: "[data-menuview=config]", anchor: ".collapsible" });
    },
});
