/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import "./styles.css";

import { showNotification } from "api/ux";
import { createPluginStore, loaded, patch } from "client";
import Button from "components/Button";
import { BaseTexture } from "pixi.js";
import { createEffect, createSignal, For, Index, JSX, on, ParentProps, Show } from "solid-js";
import { definePlugin } from "types";
import { bre } from "utils";

const [store, setStore] = createPluginStore({
    active: null as null | number,
    skins: [] as Skin[],
});

const urlCache = new Map<Blob, string>();

const getBlobUrl = (blob: Blob) => {
    if (!urlCache.has(blob))
        urlCache.set(blob, URL.createObjectURL(blob));
    return urlCache.get(blob)!;
};

type SkinBlobs = [minos: Blob, ghost: Blob, minosUHD: Blob, ghostUHD: Blob];

interface Skin {
    name: string;
    blobs: SkinBlobs;
}

type Mino = "z" | "l" | "o" | "s" | "i" | "j" | "t" | "d" | "gb" | "gbd" | "ghost" | "x";

interface Asset {
    url: string;
    loaded: boolean;
    loading: boolean;
    // yes; it's a mistake on their end
    basetexture: null;
    baseTexture?: BaseTexture;
    textures: Record<string, any>;
}

interface SkinSheet {
    id: string;
    name: string;
    assets: Record<"hd" | "uhd", Asset>;
    format: "simple" | "connected";
}

interface MinosSheet extends SkinSheet {
    colors: Record<"base" | "glow", Record<Exclude<Mino, "ghost" | "x">, number>>;
}

interface GhostSheet extends SkinSheet {
    colorizeGhost: boolean;
    colorizeX: boolean;
}

type Sheets<S extends SkinSheet> = Record<string, S> & { tetrio: S };

const toBitmap = (file: File) =>
    createImageBitmap(file).catch(async (e) => {
        // `createImageBitmap` mysteriously fails with some SVGs that otherwise render properly
        // this is a bit of an ugly hack, but oh well
        // it also likes to still not work until you try again
        if (file.type === "image/svg+xml") {
            const text = await file.text();
            const img = <img src={"data:image/svg+xml;base64," + btoa(text)} /> as HTMLImageElement;
            const canvas = <canvas height={img.height} width={img.width} /> as HTMLCanvasElement;
            const ctx = canvas.getContext("2d")!;
            ctx.drawImage(img, 0, 0);
            return createImageBitmap(canvas);
        }
        throw e;
    });

const toBlob = (canvas: HTMLCanvasElement) =>
    new Promise<Blob>((resolve, reject) => {
        canvas.toBlob((blob) => {
            if (!blob)
                reject();
            else
                resolve(blob);
        });
    });

const canvasSheet = (size: number) => {
    const canvas = <canvas width={size} height={size} /> as HTMLCanvasElement;

    return [canvas, canvas.getContext("2d")!] as [HTMLCanvasElement, CanvasRenderingContext2D];
};

const restitchSkin = (bmp: ImageBitmap) => {
    const tiles = bmp.width / bmp.height | 0;
    const spacing = (bmp.width - tiles * bmp.height) / tiles;
    const size = bmp.height;

    const [minos, minosCtx] = canvasSheet(256);
    const [ghost, ghostCtx] = canvasSheet(256);
    const [minosUHD, minosCtxUHD] = canvasSheet(512);
    const [ghostUHD, ghostCtxUHD] = canvasSheet(512);

    let tile = 0;

    const draw = (x: number, y: number, ghost?: boolean) => {
        const [a, b] = ghost
            ? [ghostCtx, ghostCtxUHD]
            : [minosCtx, minosCtxUHD];

        // aliasing hacks
        a.drawImage(bmp, tile, 0, size, size, 48 * x, 48 * y, 48, 48);
        b.drawImage(bmp, tile, 0, size, size, 96 * x, 96 * y, 96, 96);

        // actual tile
        a.drawImage(bmp, tile, 0, size, size, 1 + 48 * x, 1 + 48 * y, 46, 46);
        b.drawImage(bmp, tile, 0, size, size, 2 + 96 * x, 2 + 96 * y, 92, 92);
        tile += size + spacing;
    };

    for (let i = 0; i < 7; i++)
        draw(i % 5, i / 5 | 0);

    draw(0, 0, true);

    for (let i = 2; i < 5; i++)
        draw(i, 1);

    draw(1, 0, true);

    return Promise.all([minos, ghost, minosUHD, ghostUHD].map(toBlob)) as Promise<SkinBlobs>;
};

const injectSkin = (urls: string[], minos: MinosSheet, ghost: GhostSheet) => {
    let url = 0;
    for (const asset of [
        minos.assets.hd,
        ghost.assets.hd,
        minos.assets.uhd,
        ghost.assets.uhd,
    ]) {
        asset.baseTexture?.dispose();
        delete asset.baseTexture;
        asset.textures = {};
        asset.loading = false;
        asset.loaded = false;
        asset.url = urls[url++];
    }
};

const SkinPreview = (props: ParentProps<{
    name: string;
    minos: string;
    ghost: string;
    setSkin?: JSX.EventHandlerUnion<HTMLDivElement, MouseEvent>;
    deleteSkin?: JSX.EventHandlerUnion<HTMLDivElement, MouseEvent>;
    renameSkin?: JSX.EventHandlerUnion<HTMLInputElement, InputEvent>;
}>) =>
    <div class="control_group flex-row sc-skin">
        <Show when={props.renameSkin} fallback={<h1>{props.name}</h1>}>
            <input
                autocomplete="off" spellcheck={false}
                class="rg_target_pri"
                value={props.name}
                onInput={props.renameSkin}
            />
        </Show>
        {props.children}
        <For each={Array.from(Array(10), (_, i) => [i % 5, i / 5 | 0])}>
            {([x, y]) =>
                <div class="sc-skin-mino" style={`background: url(${props.minos}) ${-x * 48 - 1}px ${-y * 48 - 1}px`} />}
        </For>
        <For each={[[0, 0], [1, 0]]}>
            {([x, y]) =>
                <div class="sc-skin-mino" style={`background: url(${props.ghost}) ${-x * 48 - 1}px ${-y * 48 - 1}px`} />}
        </For>
        <div class="button_tr_h flex-row">
            <Show when={props.setSkin}>
                <Button class="button_tr_i" callback={props.setSkin!}>
                    set skin
                </Button>
            </Show>
            <Show when={props.deleteSkin}>
                <Button class="button_tr_i" callback={props.deleteSkin!}>
                    delete
                </Button>
            </Show>
        </div>
    </div>;

const ImportButton = (props: ParentProps<{ callback: (files: FileList) => void }>) => {
    let upload: HTMLInputElement;

    return <>
        <Button callback={() => upload.click()}>
            {props.children}
        </Button>
        <input
            ref={upload!} type="file" accept="image/*" multiple hidden
            onChange={() => props.callback(upload.files!)}
        />
    </>;
};

const SkinSettings = (props: {
    sheets: Sheets<MinosSheet>;
    ghostSheets: Sheets<GhostSheet>;
    defaultUrls: string[];
}) => {
    const [importQueue, setImportQueue] = createSignal([] as File[]);

    createEffect(on(importQueue, async () => {
        for (const file of importQueue()) {
            try {
                const blobs = await restitchSkin(await toBitmap(file));
                const name = file.name.replace(/\.\w+$/, "");
                setStore("skins", (s) => [...s, { name, blobs }]);
            } catch {
                showNotification({
                    msg: `failed to import '${file.name}' - invalid skin?`,
                    color: "#ffc600",
                    icon: "warning",
                });
            }
        }
    }));

    const activeSkin = () =>
        store.active != null ? store.skins[store.active] : null;

    const setSkin = (index: number | null) => {
        injectSkin(
            index != null
                ? store.skins[index].blobs.map(getBlobUrl)
                : props.defaultUrls,
            props.sheets.tetrio,
            props.ghostSheets.tetrio
        );

        setStore("active", index);
    };

    const deleteSkin = (index: number) => {
        for (const blob of store.skins[index].blobs) {
            if (urlCache.has(blob)) {
                URL.revokeObjectURL(urlCache.get(blob)!);
                urlCache.delete(blob);
            }
        }

        setStore("active", (n) =>
            n != null && n > index ? n - 1 : n);

        setStore("skins", (s) => {
            s.splice(index, 1);
            return [...s];
        });
    };

    const renameSkin = (index: number, e: { currentTarget: HTMLInputElement }) =>
        setStore("skins", index, "name", e.currentTarget.value);

    return (
        <>
            <hr />

            <h2>active skin</h2>
            <Show when={store.active != null} fallback={
                <SkinPreview
                    name="TETR.IO"
                    minos={props.defaultUrls[0]}
                    ghost={props.defaultUrls[1]}
                />
            }>
                <SkinPreview
                    name={activeSkin()!.name}
                    minos={getBlobUrl(activeSkin()!.blobs[0])}
                    ghost={getBlobUrl(activeSkin()!.blobs[1])}
                    renameSkin={[renameSkin, store.active]}
                />
            </Show>

            <h2>your skins</h2>
            <Show when={store.active != null}>
                <SkinPreview
                    name="TETR.IO"
                    minos={props.defaultUrls[0]}
                    ghost={props.defaultUrls[1]}
                    setSkin={[setSkin, null]}
                />
            </Show>
            <Index each={store.skins}>
                {(skin, index) =>
                    <Show when={index !== store.active}>
                        <SkinPreview
                            name={skin().name}
                            minos={getBlobUrl(skin().blobs[0])}
                            ghost={getBlobUrl(skin().blobs[1])}
                            setSkin={[setSkin, index]}
                            deleteSkin={[deleteSkin, index]}
                            renameSkin={[renameSkin, index]}
                        />
                    </Show>
                }
            </Index>

            <div class="flex-row">
                <ImportButton callback={(files) => setImportQueue([...files])}>
                    import skin
                </ImportButton>
            </div>
        </>
    );
};

export default definePlugin({
    name: "Skinning",
    description: "Allows you to modify skins in-game",
    authors: ["Rini"],

    defaultUrls: [] as string[],
    sheets: {} as Sheets<MinosSheet>,
    ghostSheets: {} as Sheets<GhostSheet>,

    settings() {
        return (
            <Show when={loaded()}>
                <SkinSettings
                    sheets={this.sheets}
                    ghostSheets={this.ghostSheets}
                    defaultUrls={this.defaultUrls}
                />
            </Show>
        );
    },

    start() {
        patch(
            bre`\(\?<=const \i=\){tetrio:{id:"tetrio",name:"TETR.IO",assets:`,
            "$this.sheets=$&"
        );
        patch(
            bre`\(\?<=,\i=\){tetrio:{id:"tetrio",name:"TETR.IO",assets:`,
            "$this.ghostSheets=$&"
        );
    },

    loaded() {
        for (const asset of [
            this.sheets.tetrio.assets.hd,
            this.ghostSheets.tetrio.assets.hd,
            this.sheets.tetrio.assets.uhd,
            this.ghostSheets.tetrio.assets.uhd,
        ]) {
            this.defaultUrls.push(asset.url);
        }

        if (store.active != null) {
            injectSkin(
                store.skins[store.active].blobs.map(getBlobUrl),
                this.sheets.tetrio,
                this.ghostSheets.tetrio
            );
        }
    },
});
