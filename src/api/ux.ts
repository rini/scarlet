/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export let switchMenu: (id: string) => void;

interface Button {
    label: string;
    classes: string[];
}

export let renderProfilePopup: (obj: {
    userID?: string;
    username?: string;
    /** buttons added to the top right corner of the profile (e.g. the friend button) */
    topbuttons?: (Button & { icon?: string; title?: string; callback: (close: () => void) => void })[];
    /** buttons added to the bottom of the profile (e.g. the room management stuff) */
    buttons?: Button[];
}) => HTMLDivElement;

export let renderDialog: (obj: {
    title?: string;
    msg?: string;
    classes?: string[];
    buttons: (Button & { id?: string; callback: (close: () => void) => void })[];
}) => void;

export let showNotification: (obj: string | {
    msg: string;
    suppressable?: boolean;
    color?: string;
    subcolor?: string;
    bgcolor?: string;
    fgcolor?: string;
    id?: string;
    classes?: string[];
    icon?: string;
    subicon?: string;
    header?: string;
    buttons?: (Button & { icon?: string; onclick: (close: () => void) => void })[];
    timeout?: number;
    onclick?: (close: () => void) => void;
}) => HTMLDivElement | undefined;

export const _init = (...a: any[]) =>
    [switchMenu, renderProfilePopup, renderDialog, showNotification] = a;
