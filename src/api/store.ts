/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// basically idb-keyval but smaller https://github.com/jakearchibald/idb-keyval

const DB_NAME = "Scarlet";

const promisifyRequest = <T>(req: IDBRequest<T>) =>
    new Promise<T>((resolve, reject) => {
        req.onsuccess = () => resolve(req.result);
        req.onerror = () => reject(req.error);
    });

const dbReq = indexedDB.open(DB_NAME);
const db = promisifyRequest(dbReq);
dbReq.onupgradeneeded = () => dbReq.result.createObjectStore("d");

const transaction = async <T>(cb: (store: IDBObjectStore) => Promise<T>, mode: IDBTransactionMode = "readwrite") =>
    cb((await db).transaction("d", mode).objectStore("d"));

export const get = <T = any>(key: IDBValidKey) =>
    transaction((store) => promisifyRequest<T>(store.get(key)), "readonly");

export const set = (key: IDBValidKey, value: any) =>
    transaction((store) =>
        promisifyRequest(store.put(value, key)).then(() => undefined)
    );

export const update = <T = any, R = T>(key: IDBValidKey, cb: (value: T) => R) =>
    transaction(async (store) => {
        const value = cb(await promisifyRequest(store.get(key)));
        await promisifyRequest(store.put(value, key));
        return value;
    });

export const nuke = () =>
    promisifyRequest<any>(indexedDB.deleteDatabase(DB_NAME)) as Promise<void>;
