/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export const Builtins = new Set(["UX"]);

/**
 * Mapping of Scarlet authors to TETR.IO user IDs. Add yourself here if you contribute! If you'd rather keep your
 * profile private, other formats can be discussed
 * You can find your own ID on the bottom of the right sidebar of your user page
 */
export const Authors = {
    Rini: "64640304016672561550b2cd",
} as const satisfies Record<string, string>;
