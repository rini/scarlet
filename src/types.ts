/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Authors } from "api/constants";
import { Component } from "solid-js";

export function definePlugin<P extends Plugin>(plug: P) {
    return plug;
}

export type Author = keyof typeof Authors;

export interface Settings {
    enabledPlugins: Set<string>;
    plugins: {
        [id: string]: Record<string, any>;
    };
}

export interface Plugin extends Record<string, any> {
    name: string;
    authors: Author[];
    description: string;
    settings?: Component;

    start?: () => void;
    loaded?: () => void;
}
