/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { execSync } from "child_process";
import esbuild from "esbuild";
import { solidPlugin } from "esbuild-plugin-solid";
import { appendFile, copyFile, readdir, readFile } from "fs/promises";
import { join } from "path";

const release = process.argv.includes("--release");
const watch = process.argv.includes("--watch") || process.argv.includes("-w");
const gitHash = execSync("git rev-parse HEAD", { encoding: "utf8" }).trim();
const gitHashShort = execSync("git rev-parse --short HEAD", { encoding: "utf8" }).trim();
const version = process.env.npm_package_version! + (release ? "" : `+git.${gitHashShort}`);

const metadataBlock = (data: Record<string, string>) =>
    [
        "// ==UserScript==",
        ...Object.entries(data).map(([k, v]) => `// @${k.padEnd(14)} ${v}`),
        "// ==/UserScript==",
        "",
    ].join("\n");

const userscriptMeta = metadataBlock({
    name: "Scarlet",
    description: "Lightweight and extensible client mod for TETR.IO",
    grant: "GM_addStyle",
    version,
    namespace: "https://codeberg.org/rini",
    match: "*://tetr.io/*",
    "run-at": "document-start",
});

const esbuildOptions: esbuild.BuildOptions = {
    bundle: true,
    minify: release,
    treeShaking: true,
    sourcemap: release ? "external" : "inline",
    logLevel: "info",
    globalName: "Scarlet",
    supported: {
        // transform SCSS-like nesting: https://www.w3.org/TR/css-nesting-1/
        nesting: false,
    },
    define: {
        SC_VERSION: JSON.stringify(version),
        SC_RELEASE: JSON.stringify(release),
        SC_GIT_FULL: JSON.stringify(gitHash),
        SC_GIT_SHORT: JSON.stringify(gitHashShort),
    },
    plugins: [
        solidPlugin(),
        {
            name: "pack-plugins",
            setup(build) {
                build.onResolve({ filter: /^~plugins$/ }, ({ path }) => ({
                    namespace: "plugins",
                    path,
                }));

                build.onLoad({ filter: /^~plugins$/, namespace: "plugins" }, async () => {
                    const plugins = [] as [string, string][];
                    for (const dir of ["src/plugins", "src/userplugins"]) {
                        const files = await readdir(dir, { withFileTypes: true }).catch(() => []);
                        for (const f of files) {
                            const pluginPath = join(dir, f.name);
                            if (f.name.startsWith("."))
                                continue;
                            if (f.isDirectory()) {
                                const files = await readdir(pluginPath);
                                if (!files.some((f) => /^index\.(tsx?|jsx?)$/.test(f)))
                                    continue;
                            } else if (!/\.(tsx?|jsx?)$/.test(f.name)) {
                                continue;
                            }
                            plugins.push([f.name.match(/^[^.]*/)![0], pluginPath]);
                        }
                    }

                    return {
                        contents: `export default {${
                            plugins
                                .map(([id, path]) =>
                                    `${id}: () => require("./${path}").default`)
                                .join(", ")
                        }}`,
                        resolveDir: ".",
                    };
                });
            },
        },
    ],
};

const targets: esbuild.BuildOptions[] = [
    {
        entryPoints: ["src/index.ts"],
        outfile: "dist/client.js",
    },
    {
        entryPoints: ["src/index.ts"],
        outfile: "dist/Scarlet.user.js",
        define: { window: "unsafeWindow" },
        banner: { js: userscriptMeta },
        footer: { js: 'Object.defineProperty(unsafeWindow,"Scarlet",{get:()=>Scarlet})' },
        plugins: [{
            name: "load-css",
            setup(build) {
                build.onEnd(async () => {
                    const css = await readFile("dist/Scarlet.user.css", "utf8");
                    await appendFile("dist/Scarlet.user.js", `;GM_addStyle(\`${css.replace(/`/g, "\\`")}\`)\n`);
                });
            },
        }],
    },
    {
        entryPoints: ["app/index.ts"],
        outfile: "dist/app.js",
        platform: "node",
        external: ["electron"],
    },
    {
        entryPoints: ["app/preload.ts"],
        outfile: "dist/preload.js",
        platform: "node",
        external: ["electron"],
    },
];

const merge = (a: object, b: object) => {
    const out = { ...a };
    for (const key in b)
        out[key] = typeof b[key] == "object" && b[key]
            ? Array.isArray(b[key])
                ? [...a[key] ?? [], ...b[key]]
                : { ...a[key], ...b[key] }
            : b[key];
    return out;
};

if (watch) {
    for (const target of targets) {
        (await esbuild.context(merge(esbuildOptions, target))).watch();
    }
} else {
    await Promise.all(targets.map((target) =>
        esbuild.build(merge(esbuildOptions, target))
    ));
}
