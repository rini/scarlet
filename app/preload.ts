/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { ipcRenderer, webFrame } from "electron";

(async () => {
    webFrame.executeJavaScript(await ipcRenderer.invoke("read-js"));

    if (SC_RELEASE) {
        webFrame.insertCSS(await ipcRenderer.invoke("read-css"));
    } else {
        let cssKey = webFrame.insertCSS(await ipcRenderer.invoke("read-css"));
        ipcRenderer.on("reload-css", (_, css: string) => {
            webFrame.removeInsertedCSS(cssKey);
            cssKey = webFrame.insertCSS(css);
        });
    }
})();
